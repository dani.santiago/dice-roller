package com.example.diceroller

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.diceroller.databinding.ActivityMainBinding
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        //Creo botó i textView de la vista
        //val button: Button = findViewById(R.id.button)
        //val number: TextView = findViewById(R.id.textView)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val button: Button = binding.button
        val buttonReset: Button = binding.buttonReset
        val dice1: ImageView = binding.dice1
        val dice2: ImageView = binding.dice2

        button.setOnClickListener {
            var randomNumber = (1..6).random()
            changeDiceImage(randomNumber, dice1)
            randomNumber = (1..6).random()
            changeDiceImage(randomNumber, dice2)
        }

        buttonReset.setOnClickListener {
            changeDiceImage(0, dice1)
            changeDiceImage(0, dice2)
        }

        dice1.setOnClickListener {
            var randomNumber = (1..6).random()
            changeDiceImage(randomNumber, dice1)

        }

        dice2.setOnClickListener {
            var randomNumber = (1..6).random()
            changeDiceImage(randomNumber, dice2)
        }
    }

    fun changeDiceImage(randomNumber: Int, dice: ImageView){
        val images: Array<Int> = arrayOf(R.drawable.empty_dice, R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3,
            R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6)
        dice.setImageResource(images[randomNumber])
        dice.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake))
        val mp: MediaPlayer = MediaPlayer.create(this, R.raw.onedice)
        mp.start()
        /*when(randomNumber){
            1 -> dice.setImageResource(R.drawable.dice_1)
            2 -> dice.setImageResource(R.drawable.dice_2)
            3 -> dice.setImageResource(R.drawable.dice_3)
            4 -> dice.setImageResource(R.drawable.dice_4)
            5 -> dice.setImageResource(R.drawable.dice_5)
            6 -> dice.setImageResource(R.drawable.dice_6)
            else -> dice.setImageResource(R.drawable.empty_dice)
        }*/
    }
}